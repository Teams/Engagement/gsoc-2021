<!-- Read our mentor's guidelines at https://wiki.gnome.org/Outreach/SummerOfCode/Mentors -->

# Project name:

# Mentors:
<!-- preferably gitlab/ldap usernames -->

# Description:
<!-- include the list of components (apps, libraries) that need to be touched in this project -->

# Requirements:
<!-- pre-requisites such as: knowledge of a specific programming language or technology -->

# Communication:
<!-- preferred communication channel: chat (IRC/Matrix), email, video call, etc... -->

/label ~"Project proposal"
