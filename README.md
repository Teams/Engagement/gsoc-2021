# Schedule

To get all the generic SoC milestones and the GNOME specific ones, have a look at [the full schedule on our wiki](https://wiki.gnome.org/Outreach/SummerOfCode/2021/Timeline).
